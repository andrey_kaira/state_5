import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:statepracticeprovider/colors_provider.dart';

import 'my_app.dart';

class Application extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ColorsProvider>(
        builder: (context) => ColorsProvider(),
        child: MaterialApp(
          theme: ThemeData.dark(),
          home: MyApp(),
        ));
  }
}
