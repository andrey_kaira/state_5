import 'package:flutter/material.dart';

class ColorsProvider with ChangeNotifier {
  Color _color = Colors.black;

  Color get color {
    return _color;
  }

  void changeColorOnTheRed() {
    _color = Colors.red;
    notifyListeners();
  }

  void changeColorOnTheYellow() {
    _color = Colors.yellow;
    notifyListeners();
  }

  void changeColorOnTheGreen() {
    _color = Colors.green;
    notifyListeners();
  }
}
