import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'colors_provider.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<ColorsProvider>(
      builder: (BuildContext ctx, ColorsProvider snap, Widget _) => Scaffold(
        appBar: AppBar(
          title: Text('State Management Practice #5'),
        ),
        drawer: Drawer(
          child: Column(
            children: <Widget>[
              AppBar(
                automaticallyImplyLeading: false,
              ),
              InkWell(
                child: Container(
                  color: Colors.red,
                  height: 100.0,
                  margin: EdgeInsets.all(15.0),
                ),
                onTap: snap.changeColorOnTheRed,
              ),
              InkWell(
                child: Container(
                  color: Colors.yellow,
                  height: 100.0,
                  margin: EdgeInsets.all(15.0),
                ),
                onTap: snap.changeColorOnTheYellow,
              ),
              InkWell(
                child: Container(
                  color: Colors.green,
                  height: 100.0,
                  margin: EdgeInsets.all(15.0),
                ),
                onTap: snap.changeColorOnTheGreen,
              ),
            ],
          ),
        ),
        body: Container(
          color: snap.color,
        ),
      ),
    );
  }
}
